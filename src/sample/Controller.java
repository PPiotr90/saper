package sample;

import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.TextAlignment;

import java.util.Collections;
import java.util.function.Consumer;


public class Controller {

    @FXML
    Label labelEmotikon;
    @FXML
    GridPane gridPane;
    @FXML
    Label label1;
    @FXML
    Label label2;
    @FXML
    Label labelScore;
    @FXML
            Button buttonMoreTime;
    int[] xBombs = new int[20];
    int[] yBombs = new int[20];
    int clear =0;
    int flags =0;
    String smile = "images/smile.jpg";
    String sad= "images/sad.jpg";
    String sunglasses= "images/sunglasses.jpg";
    String wow= "images/wow.jpg";
    ImageView main = new ImageView(smile);
    boolean play = true;

    public void initialize() {
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setGridLinesVisible(true);

        labelEmotikon.setGraphic(main);
        this.score();

        for (int i = 0; i < 20; i++) {
            int x = (int) (Math.random() * 10);
            int y = (int) (Math.random() * 10);
            xBombs[i] = x;
            yBombs[i] = y;

            String url = "images/bomb.jpg";
            ImageView imageView= new ImageView(url);
            imageView.setFitWidth(30);
            imageView.setFitHeight(22);
            gridPane.add(imageView, x, y);
        }
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (this.isNoBomb(i, j)) {
                    int value = 0;
                    if (!(this.isNoBomb(i - 1, j))) value++;
                    if (!(this.isNoBomb(i + 1, j))) value++;
                    if (!(this.isNoBomb(i, j + 1))) value++;
                    if (!(this.isNoBomb(i, j - 1))) value++;
                    if (!(this.isNoBomb(i - 1, j - 1))) value++;
                    if (!(this.isNoBomb(i + 1, j - 1))) value++;
                    if (!(this.isNoBomb(i - 1, j + 1))) value++;
                    if (!(this.isNoBomb(i + 1, j + 1))) value++;
                    Label label = new Label();
                    label.setAlignment(Pos.CENTER);
                    label.setTextAlignment(TextAlignment.CENTER);
                    label.setText(Integer.toString(value));
                    label.setAlignment(Pos.CENTER);

                    gridPane.add(label, i, j);

                }


            }
        }

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                Button button = new Button();
                int finalI = i;
                int finalJ = j;
                button.setOnAction(event -> {
                    if(this.isNoBomb(finalI, finalJ)) {
                        button.setVisible(false);
                        clear++;
                        flags--;
                        this.score();
                        if(play) main.setImage(new Image(smile));
                        labelEmotikon.setGraphic(main);

                }
                    else {
                        if(play) main.setImage(new Image(sad));
                        if(play) labelScore.setText("Przegrana!!!!");
                        button.setVisible(false);

                       labelEmotikon.setGraphic(main);
                       play=false;


                    }
                });




                button.setOnMouseReleased(event -> {
                    String url = "images/flag.jpg";
                    ImageView imageView = new ImageView(url);
                    imageView.setFitWidth(20);
                    imageView.setFitHeight(22);
                    flags++;
                    this.score();
                    button.setGraphic(imageView);
                    if(play) main.setImage(new Image(smile));
                    labelEmotikon.setGraphic(main);
                });
                button.setOnMousePressed(event -> {
                    if(play)main.setImage(new Image(wow));
                    labelEmotikon.setGraphic(main);
                });
                main.setImage(new Image(smile));
                labelEmotikon.setGraphic(main);
                button.setMaxSize(50, 45);
                gridPane.add(button, i, j);
            }
        }
        buttonMoreTime.setOnAction(event -> {

                    gridPane.getChildren().remove(0,gridPane.getChildren().size()-1);
                    labelScore.setText("");
                    clear=0;
                    flags=0;
            this.initialize();
            play=true;
            gridPane.setGridLinesVisible(true);
        });

    }

    private boolean isNoBomb(int x, int y) {
        boolean result = true;
        for (int i = 0; i < xBombs.length; i++) {
            if (x == xBombs[i] && y == yBombs[i]) {
                result = false;
                break;
            }

        }

        return result;
    }
    public  void  score(){
        label1.setText(Integer.toString(80-clear));
        label2.setText(Integer.toString(20-flags));
        if(clear==80){
            if(play) labelScore.setText("Wygrana!!");
            if(play) main.setImage(new Image(sunglasses));
            labelEmotikon.setGraphic(main);
            play=false;
        }
    }
}

